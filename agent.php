<?php
include './includes/class-autoload.inc.php';

session_start();
if (!isset($_SESSION['identifiant'])){
    header('Location: index.php');

};
?>

<!DOCTYPE php>
<html lang="fr">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css2?family=Special+Elite&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./asset/myCss.css">
  <title>MFN</title>



</head>
<body>
<div id="deco"> Déconnexion </div>
<div id="navBar">
    <div id="navAgents" onclick="location.href='agent.php';" class="navico"><img id="icoAgent" src="./asset/images/incognito.png" alt=""> Agents</div>
    <div id="navCible"  onclick="location.href='cibles.php';" class="navico"><img id="icoCible" src="./asset/images/folder.png" alt=""> Cibles</div>
    <div id="navPlanque" onclick="location.href='planques.php';" class="navico"><img id="icoPlanque" src="./asset/images/safebox.png" alt=""> Planques</div>
    <div id="navContact" onclick="location.href='contacts.php';" class="navico"><img id="icoContact" src="./asset/images/walkietalkie.png" alt=""> Contacts</div>
    <div id="navMission" onclick="location.href='mission.php';" class="navico"><img id="icoMission" src="./asset/images/clipboard.png" alt=""> Missions</div>
</div>
<div id="fondAgent">

<!--<img id='logoMfn' src='./asset/images/agent.png' alt="logoMFN">-->
<div id="ongletagents">

    <label for='nomAgent'>Nom de l'agent</label><input id='nomAgent' class='formAgent' type='text' placeholder="Nom de l'agent" name="nomAgent">
    <label for='prenomAgent'>Prénom</label><input id='prenomAgent' class='formAgent' type='text' placeholder="Prénom de l'agent" name="prenomAgent">
    <label for='naissanceAgent'>Date de naissance</label><input id='naissanceAgent' class='formAgent' type='date' placeholder="Date de Naissance" name="naissanceAgent">
    <label for='nationaliteAgent'>Pays de naissance</label><select id='nationaliteAgent' class='formAgent custom-select' type='text' placeholder="Nationalité" name="nationaliteAgent">
  <?php
  $insertPays = new GetPays();
  $insertPays-> listePays();
  ?></select>
    <label for='specialite'>Spécialité</label><select id='specialite' class='formAgent custom-select'  placeholder="Specialite"  name="specialiteAgent">
  <?php
  $insertPays = new GetSpe();
  $insertPays-> listeSpe();
  ?> </select>




<div type="submit" id="btnPhpAgent"> Envoyer </div>
</div>


    <div id="cadreReponse2">
        <ul>
            <li id='nomAgentLi'>Helo</li>
            <li id='prenomAgentLi'></li>
            <li id='naissanceAgentLi'></li>
            <li id='nationAgentLi'></li>
            <li id='specialiteAgentLi'></li>
            <li id='idAgentLi'></li>

        </ul>
    </div>
  <div id='cadreReponse'>
<div id='infosAgent'>
    <img src="" id="imgAgent">

    <div id="creePhoto"> Photo Aleatoire</div>

</div>

  </div>

    <div id='listeDesAgents'> Liste des Agents <br>
  <br>
  <?php
$listeAgent = new ListeAgents();
$listeAgent-> getAgents();
?>
</div>

</div>
</div>


  <script src="asset/jquery.min.js"></script>
  <script src="asset/customJs.js"></script>
</body>
</html>