<?php
include './includes/class-autoload.inc.php';

session_start();
if (!isset($_SESSION['identifiant'])){
    header('Location: index.php');

};
?>
<!doctype html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css2?family=Special+Elite&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="./asset/myCss.css">
  <title>MFN</title>
</head>

<body>
<div id="deco"> Déconnexion </div>

<div id="navBar">
    <div id="navAgents" onclick="location.href='agent.php';" class="navico"><img id="icoAgent" src="./asset/images/incognito.png" alt=""> Agents</div>
    <div id="navCible"  onclick="location.href='cibles.php';" class="navico"><img id="icoCible" src="./asset/images/folder.png" alt=""> Cibles</div>
    <div id="navPlanque" onclick="location.href='planques.php';" class="navico"><img id="icoPlanque" src="./asset/images/safebox.png" alt=""> Planques</div>
    <div id="navContact" onclick="location.href='contacts.php';" class="navico"><img id="icoContact" src="./asset/images/walkietalkie.png" alt=""> Contacts</div>
    <div id="navMission" onclick="location.href='mission.php';" class="navico"><img id="icoMission" src="./asset/images/clipboard.png" alt=""> Missions</div>
</div>
  <div id="fondAgent">
    <div id="ongletagents">

      <input id="nomContact" class="formContact" type="text" placeholder="Nom du Contact" name="nomContact">
      <input id="prenomContact" class="formContact" type="text" placeholder="Prenom du Contact" name="prenomContact">
      <input id="naissanceContact" class="formContact" type="date" placeholder="Date de naissance" name="naissanceContact">
      <select id="paysContact" class="formContact custom-select" type="text" placeholder="Pays de naissance" name="paysContact">
        <?php
        $insertPays = new GetPays();
        $insertPays->listePays();
        ?> </select>
      <button type='submit' id="btnPhpContact"> Envoyer </button>
  </div>

      <div id="cadreReponse2">
          <ul>
              <li id='nomAgentLi'></li>
              <li id='prenomAgentLi'></li>
              <li id='naissanceAgentLi'></li>
              <li id='nationAgentLi'></li>
              <li id='specialiteAgentLi'></li>
              <li id='idAgentLi'></li>

          </ul>
      </div>

  <div id="cadreReponse"> Reponse de la requete :
      <div id='infosAgent'>
          <img src="" id="imgAgent">

          <div id="creePhoto"> Photo Aleatoire</div>

      </div>
  </div>
  <div id="listeDesAgents"> Liste des Contacts <br> <br>

    <?php
    $testObj = new ListeContacts();
    $testObj->getContacts();

    ?>

  </div>

  </div>




  <script src="asset/jquery.min.js"></script>
  <script src="asset/customJs.js"></script>
</body>

</html>