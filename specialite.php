<?php
include './includes/class-autoload.inc.php';
?>

<!doctype html>
<html lang="fr">

<head>
    <title>MFN</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="asset/myCss.css">

</head>

<body>

    <div class="container">
        <div id='logoMfn'></div>
        <div id="ongletSpecialite">

            <input id="genreSpecialite" class="formSpecialite" type="text" placeholder="specialite" name="genreSpecialite">
            <input id="idSpecialite" class="formSpecialite" type="text" placeholder="id specialite" name="idSpecialite">
            <div id="btnPhpSpecialite">Envoyer</div>

            <div id="cadreReponse"> Reponse de la requete : </div>
            <div id="listeSpecialites"> Liste de Specialités <br> <br>

                <?php
                $testObj = new GetSpecialite();
                $testObj->getSpecialite();

                ?>

            </div>

        </div>

    </div>




    <script src="asset/jquery.min.js"></script>
    <script src="asset/myJs.js"></script>
</body>

</html>