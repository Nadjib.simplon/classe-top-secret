<?php 

spl_autoload_register('myAutoLoader');

function myAutoLoader($className) {
    $path = 'class/';
    $extention = '.class.php';
    $fileName = $path . $className . $extention;



    if (!file_exists($fileName)) {
        return false ;

    }

    include_once $path . $className . $extention;
}