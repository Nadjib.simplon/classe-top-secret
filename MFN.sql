START TRANSACTION;
DROP DATABASE IF EXISTS MFN;
CREATE DATABASE MFN;
USE MFN;

CREATE TABLE pays (
id_pays INT NOT NULL AUTO_INCREMENT,
pays VARCHAR (25),
PRIMARY KEY (id_pays)
);

INSERT INTO pays (pays) VALUES ("Republique Tcheque");
INSERT INTO pays (pays) VALUES ("Cuba");
INSERT INTO pays (pays) VALUES ("Vietnam");
INSERT INTO pays (pays) VALUES ("Rwanda"); 

CREATE TABLE status (
id_status INT NOT NULL AUTO_INCREMENT,
libelle_status VARCHAR (25),
PRIMARY KEY (id_status)
);


CREATE TABLE spe_agent (
id_spe_agent INT NOT NULL AUTO_INCREMENT,
id_agent INT,
PRIMARY KEY (id_spe_agent)
);

CREATE TABLE admin (
identifiant VARCHAR (25),
mdp VARCHAR (25)
);

INSERT INTO admin VALUES ("Nadjib","azerty");
INSERT INTO admin VALUES ("Estefania","azerty");
INSERT INTO admin VALUES ("Thomas","azerty");

CREATE TABLE specialite (
id_spe INT NOT NULL AUTO_INCREMENT,
genre VARCHAR (25),
PRIMARY KEY (id_spe),
CONSTRAINT fk_specialite_spe_agent FOREIGN KEY (id_spe) REFERENCES spe_agent(id_spe_agent)
);

CREATE TABLE type_planque (
id_planque INT NOT NULL AUTO_INCREMENT,
genre VARCHAR(25), 
PRIMARY KEY (id_planque)
);

INSERT INTO type_planque(genre) VALUES ("Hotel");
INSERT INTO type_planque(genre) VALUES ("Appartement");
INSERT INTO type_planque(genre) VALUES ("Sous-sol");
INSERT INTO type_planque(genre) VALUES ("Bunker");

CREATE TABLE type_mission (
id_mission INT NOT NULL AUTO_INCREMENT,
genre VARCHAR(25), 
PRIMARY KEY (id_mission)
);

INSERT INTO type_mission (genre) VALUES ("Operation clandestine");
INSERT INTO type_mission (genre) VALUES ("Contre-espionnage");
INSERT INTO type_mission (genre) VALUES ("Cyber-espionnage");
INSERT INTO type_mission (genre) VALUES ("Infiltration");

CREATE TABLE contacts (
id_contact INT NOT NULL AUTO_INCREMENT,
nom VARCHAR(25), 
prenom VARCHAR (25), 
naissance DATE, 
pays INT,
PRIMARY KEY (id_contact),
CONSTRAINT fk_contacts_pays FOREIGN KEY (pays) REFERENCES pays(id_pays)
);
INSERT INTO contacts (nom, prenom) VALUES ("Kafka", "Franz");
INSERT INTO contacts (nom, prenom) VALUES ("Curie", "Marie");
INSERT INTO contacts (nom, prenom) VALUES ("Kepler", "Johannes");
INSERT INTO contacts (nom, prenom) VALUES ("Kahlo", "Frida");

CREATE TABLE planques (
id_planque INT NOT NULL AUTO_INCREMENT, 
adresse VARCHAR(25), 
pays INT, 
type_planque INT, 
PRIMARY KEY (id_planque),
CONSTRAINT fk_planques_pays FOREIGN KEY (pays) REFERENCES pays(id_pays),
CONSTRAINT fk_planques_type_planque FOREIGN KEY (type_planque) REFERENCES type_planque (id_planque)
);

CREATE TABLE agents (
id_agent INT NOT NULL AUTO_INCREMENT,
nom VARCHAR(25), 
prenom VARCHAR (25), 
naissance DATE, 
pays INT,
spe_agent INT,
PRIMARY KEY (id_agent),
CONSTRAINT fk_agent_pays FOREIGN KEY (pays) REFERENCES pays (id_pays)
);
INSERT INTO agents (nom, prenom) VALUES ("Mad", "Max");
INSERT INTO agents (nom, prenom) VALUES ("Balboa", "Rocky");
INSERT INTO agents (nom, prenom) VALUES ("Organa", "Leia");
INSERT INTO agents (nom, prenom) VALUES ("Berry", "Halle"); 

CREATE TABLE cibles (
id_cible INT NOT NULL AUTO_INCREMENT, 
nom VARCHAR(25), 
prenom VARCHAR (25), 
naissance DATE, 
pays INT,
PRIMARY KEY (id_cible),
CONSTRAINT fk_cibles_pays FOREIGN KEY (pays) REFERENCES pays(id_pays)
);
INSERT INTO cibles VALUES (1,"Rockatansky", "Max", "2020-01-01",1);

CREATE TABLE mission (
id_mission INT AUTO_INCREMENT NOT NULL,
titre VARCHAR(40),
description VARCHAR(255),
type_mission INT,
status INT,
agents INT,
specialite INT,
cibles INT,
contact INT,
planque INT,
pays INT,
spe_agent INT,
PRIMARY KEY (id_mission),
CONSTRAINT fk_mission_type_mission FOREIGN KEY (type_mission) REFERENCES type_mission(id_mission),
CONSTRAINT fk_mission_status FOREIGN KEY (status) REFERENCES status(id_status),
CONSTRAINT fk_mission_agents FOREIGN KEY (agents) REFERENCES agents(id_agent),
CONSTRAINT fk_mission_specialite FOREIGN KEY (specialite) REFERENCES specialite(id_spe),
CONSTRAINT fk_mission_cibles FOREIGN KEY (cibles) REFERENCES cibles(id_cible),
CONSTRAINT fk_mission_contact FOREIGN KEY (contact) REFERENCES contacts(id_contact),
CONSTRAINT fk_mission_planque FOREIGN KEY (planque) REFERENCES planques(id_planque),
CONSTRAINT fk_mission_pays FOREIGN KEY (pays) REFERENCES pays(id_pays),
CONSTRAINT fk_mission_spe_agent FOREIGN KEY (spe_agent) REFERENCES spe_agent(id_spe_agent)
);


COMMIT;
