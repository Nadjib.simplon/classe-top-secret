## BRIEF
site internet permettant la gestion des données du KGB.
<br/>

## TECHNO-TAGS
- HTML5
- CSS3
- SASS
- JavaScript
- ES6
- PHP7
- MySQL
- Jquery/ Ajax


## AUTEURS
- Nadjib Chaibeddra et Estefania Vila
