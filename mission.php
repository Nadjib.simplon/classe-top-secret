<?php
include './includes/class-autoload.inc.php';

session_start();
if (!isset($_SESSION['identifiant'])){
    header('Location: index.php');

};
?>

<!doctype html>
<html lang="fr" xmlns="http://www.w3.org/1999/html">
<head>
<link href="https://fonts.googleapis.com/css2?family=Special+Elite&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="./asset/myCss.css">

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<div id="deco"> Déconnexion </div>


<div id="navBar">
    <div id="navAgents" onclick="location.href='agent.php';" class="navico"><img id="icoAgent" src="./asset/images/incognito.png" alt=""> Agents</div>
    <div id="navCible"  onclick="location.href='cibles.php';" class="navico"><img id="icoCible" src="./asset/images/folder.png" alt=""> Cibles</div>
    <div id="navPlanque" onclick="location.href='planques.php';" class="navico"><img id="icoPlanque" src="./asset/images/safebox.png" alt=""> Planques</div>
    <div id="navContact" onclick="location.href='contacts.php';" class="navico"><img id="icoContact" src="./asset/images/walkietalkie.png" alt=""> Contacts</div>
    <div id="navMission" onclick="location.href='mission.php';" class="navico"><img id="icoMission" src="./asset/images/clipboard.png" alt=""> Missions</div>
</div>
<div id='container'>
    <div id="modalMission">
        <?php
        $listeMission = new GetMission();
        $listeMission-> listeMission();
        ?>
    <div id="photoAgent"></div>
    <div id="photoCible"></div>
    <div id="photoPlanque"></div>
    <div id="photoPays"></div>
    <div id="infosMission"></div>
    </div>

    <div id="btnOngletForm"> Crée une mission</div>

    <div id="ongletVisuMission">
        <div id="infosGenerales">
            <ol>
                <li id ='idMissionModal'> id_Mission</li>
                <li style='color:red' id ='descriptionModal'>Descritpion :</li>
                <li id ='debutModal'>Date de début : </li>
                <li id ='finModal'>Date de fin :</li>
                <li id ='specialiteModal'>Specialite :</li>
                <li id ='typeMissionModal'>Type Mission :</li>



            </ol>
        </div>
        <div id="paysModal"> </div>
        <div id="infosAgentModal">
            <ol>
                <li> --- AGENT --- </li>
            <li id="nomAgentModal">  </li>
            <li id="prenomAgentModal">  </li>
            <li id="photoAgentModal"> </li>
            </ol>

        </div>
        <div id="infosCibleModal">
            <ol>
                <li style='color:red'> --- CIBLE --- </li>

                <li id="nomCibleModal"></li>
                <li id="prenomCibleModal"></li>
                <li id="photoCibleModal"></li>
            </ol>

        </div>
        <div id="infosContactModal">
            <ol>
                <li> --- CONTACT --- </li>
                <li id="nomContactModal"></li>
                <li id="prenomContactModal"></li>
                <li id="photoContactModal">b</li>
            </ol>

        </div>
        <div id="infoPlanqueModal">
            <ol>
                <li> --- PLANQUE --- </li>
                <li id="adressePlanqueModal"> </li>
                <li id="typePlanqueModal"></li>
                <li id="photoPlanqueModal">a</li>
            </ol>

        </div>
    </div>
    <div id="ongletVisuMissionBtn">Selection</div>
    <div id="formuaireMission">
    <label for="inputTitreMission">Titre de la Mission : </label><input id="inputTitreMission" type="text" placeholder="Titre de la mission">
    <label for="inputDescriptionMission">Déscription de le Mission : </label><input id="inputDescriptionMission" type="text" placeholder="Descritption">
    <label for="inputDebutMission">Date de début de le mission :</label><input id="inputDebutMission" type="date" placeholder="Titre de la mission">
    <label for="inputFinMission">Date de fin de le mission :</label><input id="inputFinMission" type="date" placeholder="Titre de la mission">
    <label for="inputPaysMission">Localisation de la Mission :</label><select id="inputPaysMission">
        <?php
        $listePays = new GetPays();
        $listePays-> listePays();
        ?>
    </select>
    <label for="inputSpeMission">Spécialité requise par l'agent</label><Select id="inputSpeMission">
        <?php
        $insertSpe = new GetSpe();
        $insertSpe-> listeSpe();
        ?>
    </Select>
    <label for="inputStatusMission">Status de la mission :</label><select id="inputStatusMission">
        <?php
        $insertStatus = new GetStatus();
        $insertStatus-> listeStatus();
        ?>
    </select>
    <label for="inputTypeMission">Type de Mission :</label><select id="inputTypeMission">
        <?php
        $insertType = new GetTypeMission();
        $insertType-> listeTypeMission();
        ?>
    </select>

    <label for="inputCibleMission">Cible à désigner :</label><select id="inputCibleMission">
        <?php
        $listeCibles = new GetCible();
        $listeCibles-> listeCible();
        ?>
    </select>
    <label for="inputPlanqueMission">Planques disponibles :</label><select id="inputPlanqueMission">
        <?php
        $listePlanque = new GetPlanque();
        $listePlanque-> listePlanque();
        ?>
    </select>
    <label for="inputContactMission">Contact sur place :</label><select id="inputContactMission">
        <?php
 /*       $listeContact = new GetContact();
        $listeContact-> listeContact();*/
        ?>
    </select>
    <label for="inputAgentMission">Agent à désigner :</label><select id="inputAgentMission">
        <!--   --><?php
        /*        $listeAgent = new GetAgent();
                $listeAgent-> listeAgent();
                */?>
    </select>
<div id="btnPhpMission"> Soumettre la mission</div>
</div>
<script src="asset/jquery.min.js">

</script>
<script src="asset/customJs.js">

</script>
</body>
</html>