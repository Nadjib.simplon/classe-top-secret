START TRANSACTION;
DROP DATABASE IF EXISTS MFN;
CREATE DATABASE MFN CHARACTER SET utf8 COLLATE utf8_general_ci;
USE MFN;

CREATE TABLE pays (
id_pays INT,
pays VARCHAR (25),	
PRIMARY KEY (id_pays)
);


CREATE TABLE status (
id_status INT,
libelle_status VARCHAR (25),
PRIMARY KEY (id_status)
);


CREATE TABLE spe_agent (
id_agent INT ,
id_spe_agent INT ,
PRIMARY KEY(id_agent)
);

CREATE TABLE admin (
identifiant VARCHAR (25),
mdp VARCHAR (25)
);

CREATE TABLE specialite (
id_spe INT,
genre VARCHAR (25),
PRIMARY KEY (id_spe)

);

CREATE TABLE type_planque (
id_planque INT,
genre VARCHAR(25), 
PRIMARY KEY (id_planque)
);

CREATE TABLE type_mission (
id_mission INT,
genre VARCHAR(25), 
PRIMARY KEY (id_mission)
);

CREATE TABLE contacts (
id_contact INT,
nom VARCHAR(25), 
prenom VARCHAR (25), 
naissance DATE, 
pays INT,
photo VARCHAR(255),
PRIMARY KEY (id_contact),
CONSTRAINT fk_contacts_pays FOREIGN KEY (pays) REFERENCES pays(id_pays)
);

CREATE TABLE planques (
id_planque INT, 
adresse VARCHAR(25), 
pays INT, 
type_planque INT, 
photo VARCHAR(255),
PRIMARY KEY (id_planque),
CONSTRAINT fk_planques_pays FOREIGN KEY (pays) REFERENCES pays(id_pays),
CONSTRAINT fk_planques_type_planque FOREIGN KEY (type_planque) REFERENCES type_planque (id_planque)
);

CREATE TABLE agents (
id_agent INT,
nom VARCHAR(25), 
prenom VARCHAR (25), 
naissance DATE, 
pays INT,
photo VARCHAR(255),
PRIMARY KEY (id_agent),
CONSTRAINT fk_agent_pays FOREIGN KEY (pays) REFERENCES pays(id_pays)

);

CREATE TABLE cibles (
id_cible INT, 
nom VARCHAR(25), 
prenom VARCHAR (25), 
naissance DATE, 
pays INT,
photo VARCHAR(255),
PRIMARY KEY (id_cible),
CONSTRAINT fk_cibles_pays FOREIGN KEY (pays) REFERENCES pays(id_pays)
);

CREATE TABLE mission (
id_mission INT,
titre VARCHAR(40),
description VARCHAR(255),
debut DATE,
fin DATE,
type_mission INT,
status INT,
agents INT,
specialite INT,
cibles INT,
contact INT,
planque INT,
pays INT,
PRIMARY KEY (id_mission),
CONSTRAINT fk_mission_type_mission FOREIGN KEY (type_mission) REFERENCES type_mission(id_mission),
CONSTRAINT fk_mission_status FOREIGN KEY (status) REFERENCES status(id_status),
CONSTRAINT fk_mission_agents FOREIGN KEY (agents) REFERENCES agents(id_agent),
CONSTRAINT fk_mission_specialite FOREIGN KEY (specialite) REFERENCES specialite(id_spe),
CONSTRAINT fk_mission_cibles FOREIGN KEY (cibles) REFERENCES cibles(id_cible),
CONSTRAINT fk_mission_contact FOREIGN KEY (contact) REFERENCES contacts(id_contact),
CONSTRAINT fk_mission_planque FOREIGN KEY (planque) REFERENCES planques(id_planque),
CONSTRAINT fk_mission_pays FOREIGN KEY (pays) REFERENCES pays(id_pays)
);

ALTER TABLE spe_agent
ADD CONSTRAINT fk_speagent_specialite FOREIGN KEY (id_spe_agent) REFERENCES specialite(id_spe);
INSERT INTO status VALUES (1,"Mission disponible");
INSERT INTO status VALUES (2,"Mission en cours");
INSERT INTO status VALUES (3,"Mission terminee");
INSERT INTO admin VALUES ("Nadjib","azerty");
INSERT INTO admin VALUES ("Estefania","azerty");
INSERT INTO admin VALUES ("Thomas","azerty");
INSERT INTO specialite(id_spe,genre) VALUES (1,"Assassin");
INSERT INTO specialite(id_spe,genre) VALUES (2,"Voleur");
INSERT INTO specialite(id_spe,genre) VALUES (3,"AgentDouble");
INSERT INTO specialite(id_spe,genre) VALUES (4,"Hackeur");
INSERT INTO type_planque(id_planque,genre) VALUES (1,"Hotel");
INSERT INTO type_planque(id_planque,genre) VALUES (2,"Appartement");
INSERT INTO type_planque(id_planque,genre) VALUES (3,"Sous-sol");
INSERT INTO type_planque(id_planque,genre) VALUES (4,"Bunker");
INSERT INTO type_mission (id_mission,genre) VALUES (1,"Opération clandestine");
INSERT INTO type_mission (id_mission,genre) VALUES (2,"Contre-espionnage");
INSERT INTO type_mission (id_mission,genre) VALUES (3,"Cyber-espionnage");
INSERT INTO type_mission (id_mission,genre) VALUES (4,"Infiltration");
INSERT INTO pays (id_pays,pays) VALUES (1,"République Tchèque");
INSERT INTO pays (id_pays,pays) VALUES (2,"Cuba");
INSERT INTO pays (id_pays,pays) VALUES (3,"Vietnam");
INSERT INTO pays (id_pays,pays) VALUES (4,"Rwanda");
INSERT INTO pays(id_pays,pays) VALUES (5,"France");
INSERT INTO pays(id_pays,pays) VALUES (6,"Allemagne");
INSERT INTO pays(id_pays,pays) VALUES (7,"Russie");
INSERT INTO pays(id_pays,pays) VALUES (8,"Angleterre");

COMMIT;

