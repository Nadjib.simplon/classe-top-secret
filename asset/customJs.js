let photoRandom
let onglet = 1
let ongletMission = 1
console.log("Hello")



$('#creePhoto').click(function(){
    randomUser()
})
randomUser = () => {
    $.ajax({
        url: 'https://randomuser.me/api/',
        dataType: 'json',
        success: function(data) {
            photoRandom = data.results[0].picture.large;
            $('#randomPhoto').attr('src',photoRandom) ;
            $('#imgAgent').attr('src',photoRandom) ;


        }
    });
}

$('#icoAgent').hover(
    function(){$(this).attr('src','./asset/images/incognitoHover.png')},
    function (){$(this).attr('src','./asset/images/incognito.png')}
);

$('#icoCible').hover(
    function(){$(this).attr('src','./asset/images/folderHover.png')},
    function (){$(this).attr('src','./asset/images/folder.png')}
);
$('#icoContact').hover(
    function(){$(this).attr('src','./asset/images/walkietalkieHover.png')},
    function (){$(this).attr('src','./asset/images/walkietalkie.png')}
);
$('#icoPlanque').hover(
    function(){$(this).attr('src','./asset/images/safeboxHover.png')},
    function (){$(this).attr('src','./asset/images/safebox.png')}
);
$('#icoMission').hover(
    function(){$(this).attr('src','./asset/images/clipboardHover.png')},
    function (){$(this).attr('src','./asset/images/clipboard.png')}
);


$('.agent').click(function(e){
    $('.agent').removeClass('active')
    $(this).addClass('active');
    let codeAgent =  e.target.id;


    $.ajax({
        type: "GET",
        url: "getInfosAgents.php",
        data : {'codeAgent' : codeAgent },
        datatype: 'json',

        success: function(msg){

            $('#nomAgentLi').html(msg.nom)
            $('#prenomAgentLi').html(msg.prenom)
            $('#naissanceAgentLi').html(msg.naissance)
            $('#nationAgentLi').html(msg.pays)
            $('#specialiteAgentLi').html(msg.specialite)
            $('#imgAgent').attr('src',msg.photo) ;



        }
    })
})
$('#deco').click(function(){
    window.location.replace("deconnexion.php");

})

$('.mission').click(function(e){
    $('.mission').removeClass('active')
    $(this).addClass('active');
    togglemission()
    let codeMission =  e.target.id;
    $.ajax({
        type: "GET",
        url: "./class/ModalMission.class.php",
        data : {'codeMission' : codeMission },
        datatype: 'json',

        success: function(msg){
            console.log(msg)

           $('#idMissionModal').html('id mission  :'+msg.id_mission)
           $('#descriptionModal').html('Description  :'+msg.description)
            $('#debutModal').html('Début  :'+msg.debut)
            $('#finModal').html('Fin  :' +msg.fin)
            $('#specialiteModal').html('Spécialitée  : '+msg.specialite)
            $('#typeMissionModal').html('Type de mission  : '+msg.typeMission)
            $('#paysModal').css('background-image',`url(${msg.photoPays})`)
            $('#nomAgentModal').html("Nom :"+msg.nomAgent)
            $('#prenomAgentModal').html("Prenom :"+msg.prenomAgent)
            $('#photoAgentModal').css('background-image',`url(${msg.photoAgent})`)
            $('#nomContactModal').html("Nom :"+msg.contactNom)
            $('#prenomContactModal').html("Prenom :"+msg.contactPrenom)
            $('#photoContactModal').css('background-image',`url(${msg.contactPhoto})`)
            $('#adressePlanqueModal').html(msg.adressePlanque)
            switch (msg.typePlanque) {
                case '1':
                    $('#typePlanqueModal').html("Type : Chambre d'Hotel")
                    $('#photoPlanqueModal').css('background-image',`url(./asset/images/Hotel.jpg)`);
                    break;
                case '2':
                    $('#typePlanqueModal').html('Type : Appartement')
                    $('#photoPlanqueModal').css('background-image',`url(./asset/images/appart.jpg)`);
                    break;
                case '3':
                    $('#typePlanqueModal').html('Type : Sous-Sol')
                    $('#photoPlanqueModal').css('background-image',`url(./asset/images/sousSol.jpg)`);
                    break;
                case '4':
                    $('#typePlanqueModal').html( 'Type : Bunker')
                    $('#photoPlanqueModal').css('background-image',`url(./asset/images/bunker.jpg)`);
                    break;
                default:
                    alert("Pas d'images")
            }


            $('#photoCibleModal').css('background-image',`url(${msg.ciblePhoto})`)
            $('#nomCibleModal').html("Nom : "+msg.ciblesNom)
            $('#prenomCibleModal').html("Prenom :"+msg.ciblePrenom)





        }
    })
})

$('.cibles').click(function(e){
    $('.cibles').removeClass('active')
    $(this).addClass('active');
    let codeCible =  e.target.id;


    $.ajax({
        type: "GET",
        url: "getinfosCibles.php",
        data : {'codeCible' : codeCible },
        datatype: 'json',

        success: function(msg){

            $('#nomAgentLi').html(msg.nom)
            $('#prenomAgentLi').html(msg.prenom)
            $('#naissanceAgentLi').html(msg.naissance)
            $('#nationAgentLi').html(msg.pays)
            $('#imgAgent').attr('src',msg.photo) ;



        }
    })
})

togglemission = () => {

    if( ongletMission == 0){
        ongletMission = 1 ;
        $("#ongletVisuMissionBtn").animate({right: '902px',transition : '1s'});
        $("#ongletVisuMission").animate({right: '0px',transition : '1s'});
    }else{
        ongletMission = 0;
        $("#ongletVisuMissionBtn").animate({right: '0px',transition : '1s'});
        $("#ongletVisuMission").animate({right: '-902px',transition : '1s'});
    }
}

toggleOnglet = () => {
    if( onglet == 1){
        onglet = 0 ;
        $("#btnOngletForm").animate({left: '0px',transition : '0.5s'});
        $("#formuaireMission").animate({left: '-350px',transition : '0.5s'});
    }else{
        onglet = 1 ;
        $("#btnOngletForm").animate({left: '350px'});
        $("#formuaireMission").animate({left: '0px'});
    }
}
$('#ongletVisuMissionBtn').click(function(){
    togglemission()
})

$('#btnOngletForm').click(function(){
    toggleOnglet()
})
$('.contacts').click(function(e){
    $('.contacts').removeClass('active')
    $(this).addClass('active');
    let codeContact =  e.target.id;


    $.ajax({
        type: "GET",
        url: "getInfosContact.php",
        data : {'codeContact' : codeContact },
        datatype: 'json',

        success: function(msg){

            $('#nomAgentLi').html(msg.nom)
            $('#prenomAgentLi').html(msg.prenom)
            $('#naissanceAgentLi').html(msg.naissance)
            $('#nationAgentLi').html(msg.pays)
            $('#imgAgent').attr('src',msg.photo) ;



        }
    })
})

$('.planques').click(function(e){

    let codePlanque =  e.target.id;
    $('.planques').removeClass('active')
    $(this).addClass('active');

    $.ajax({
        type: "GET",
        url: "getinfosPlanques.php",
        data : {'codePlanque' : codePlanque },
        datatype: 'json',

        success: function(msg){
            console.log(msg)
            $('#nomAgentLi').html(msg.adresse)
            $('#prenomAgentLi').html(msg.planquePays)
            $('#naissanceAgentLi').html(msg.genre)
            $('#nationAgentLi').html(msg.pays)
            $('#imgPlanque').attr('src',msg.photo) ;



        }
    })
})

$('#typePlanque').click(function(){
    let typePlan = $('#typePlanque').val()

    switch (typePlan) {
        case '1':
            $('#imgPlanque').attr('src', './asset/images/Hotel.jpg');
            break;
        case '2':
            $('#imgPlanque').attr('src', './asset/images/appart.jpg');
            break;
        case '3':
            $('#imgPlanque').attr('src', './asset/images/sousSol.jpg');
            break;
        case '4':
            $('#imgPlanque').attr('src', './asset/images/bunker.jpg');
            break;
        default:
            alert("Pas d'images")
    }
})

$('#inputPlanqueMission').click(function() {

    let paysMission = $('#inputPaysMission').val()
    $.ajax({
        type: "POST",
        url: "./class/ContraintePaysPlanqueMission.class.php",
        dataType: 'html',
        data: {"paysMission": paysMission},

        success: function (msg) {

            $('#inputPlanqueMission').html(msg)

        }
    })
})


$('#inputContactMission').click(function() {

    let paysMission = $('#inputPaysMission').val()
    $.ajax({
        type: "POST",
        url: "./class/ContraintePaysContactMission.class.php",
        dataType: 'html',
        data: {"paysMission": paysMission},

        success: function (msg) {

            $('#inputContactMission').html(msg)

        }
    })
})
$('#inputAgentMission').click(function(){

    let speMisson = $('#inputStatusMission').val()
    let paysCible = $('#inputCibleMission').val().slice(0,1);
    $.ajax({
        type: "POST",
        url: "./class/ContraintePaysCibleAgent.class.php",
        dataType: 'html',
        data: {"paysCible":paysCible , "speMisson":speMisson},

        success: function (msg) {

            $('#inputAgentMission').html(msg)

            /*
                        location.reload();
            */

        }
    })

})
$('#btnPhpMission').click(function(){

    let titreMission = $('#inputTitreMission').val();
    let descriptionMission = $('#inputDescriptionMission').val();
    let debutMission = $('#inputDebutMission').val();
    let finMission = $('#inputFinMission').val();
    let paysMission = $('#inputPaysMission').val();
    let speMission = $('#inputSpeMission').val();
    let statusMission = $('#inputStatusMission').val();
    let typeMission = $('#inputTypeMission').val();
    let agentMission = $('#inputAgentMission').val();
    let cibleMission = $('#inputCibleMission').val().slice(1);
    let planqueMission = $('#inputPlanqueMission').val();
    let contactMission = $('#inputContactMission').val();
    let idMission = rnd(0,10000000);
    console.log(titreMission)
    console.log(descriptionMission)
    console.log(debutMission)
    console.log(finMission)
    console.log(paysMission)
    console.log(speMission)
    console.log(statusMission)
    console.log(typeMission)
    console.log(agentMission)
    console.log(cibleMission)
    console.log(planqueMission)
    console.log(contactMission)
    console.log(idMission)


    if(titreMission== '' ){alert('titre mission vide')}
    if(descriptionMission== '' ){alert('descriptionMission mission vide')}
    if(debutMission== '' ){alert('debutMission mission vide')}
    if(finMission== '' ){alert('finMission mission vide')}
    if(paysMission== '' ){alert('paysMission mission vide')}
    if(speMission== '' ){alert('speMission mission vide')}
    if(statusMission== '' ){alert('statusMission mission vide')}
    if(typeMission== '' ){alert('typeMission mission vide')}
    if(agentMission== '' ){alert('agentMission mission vide')}else{alert(agentMission)}
    if(cibleMission== '' ){alert('cibleMission mission vide')}
    if(planqueMission== '' ){alert('planqueMission mission vide')}
    if(contactMission== '' ){alert('contactMission mission vide')}
    if(idMission== '' ){alert('idMission mission vide')}else{alert(idMission)}



    $.ajax({
        type: "POST",
        url: "postMission.php",
        data: { "titreMission": titreMission, "descriptionMission": descriptionMission,
            "debutMission": debutMission, "finMission": finMission, "paysMission": paysMission,
            "speMission": speMission ,"statusMission": statusMission,
            "typeMission": typeMission, "agentMission": agentMission, "cibleMission": cibleMission,
            "planqueMission": planqueMission, "contactMission": contactMission, "idMission": idMission},

        success: function (msg) {
            alert('Mission ajoutée')
          location.reload();

        }
    })
})


$('#btnPhpAgent').click(function () {


    let nomAgent = $('#nomAgent').val();
    let prenomAgent = $('#prenomAgent').val();
    let naissanceAgent = $('#naissanceAgent').val();
    let nationaliteAgent = $('#nationaliteAgent').val();
    let idAgent = rnd(0,10000000);
    let speAgent = $('#specialite').val()
    let photoAgent = photoRandom


    $.ajax({
        type: "POST",
        url: "post.php",
        data: { "nom": nomAgent, "prenom": prenomAgent, "naissance": naissanceAgent, "pays": nationaliteAgent, "id_agent": idAgent, "spe_agent": speAgent ,"picAgent": photoAgent },

        success: function (msg) {
            location.reload();

            $('#cadreReponse').html(msg)
        }
    })
})

$('#btnPhpCible').click(function () {


    let nomCible = $('#nomCible').val();
    let prenomCible = $('#prenomCible').val();
    let naissanceCible = $('#naissanceCible').val();
    let paysCible = $('#paysCible').val();
    let idCible = rnd(0,10000000);
    let photoCible = photoRandom


    $.ajax({
        type: "POST",
        url: "postCibles.php",
        data: { "nom": nomCible, "prenom": prenomCible, "naissance": naissanceCible, "pays": paysCible, "id_cible": idCible,"photoCible":photoCible },

        success: function (msg) {
            location.reload();

            $("#cadreReponse").html(msg)
        }
    })
})

$('#btnPhpContact').click(function () {


    let nomContact = $('#nomContact').val();
    let prenomContact = $('#prenomContact').val();
    let naissanceContact = $('#naissanceContact').val();
    let paysContact = $('#paysContact').val();
    let idContact = rnd(0,10000000);
    let photoContact = photoRandom




    $.ajax({
        type: "POST",
        url: "postContacts.php",
        data: { "nom": nomContact, "prenom": prenomContact, "naissance": naissanceContact, "pays": paysContact, "id_contact": idContact ,'photoContact':photoContact },

        success: function (msg) {
            $("#cadreReponse").html(msg)
            location.reload();

        }
    })
})
function rnd(min, max)
{
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

$('#btnPhpPlanque').click(function () {
    let adressePlanque = $('#adressePlanque').val();
    let typePlanque = $('#typePlanque').val();
    let paysPlanque = $('#paysPlanque').val();
    let idPlanque = rnd(0,10000000);
    let photoPlanque = $('#inputPhotoPlanque').val();



        $.ajax({
        type: "POST",
        url: "postPlanques.php",
        data: { "idPlanque":idPlanque ,"adresse": adressePlanque, "type": typePlanque, "pays": paysPlanque,"photoPlanque": photoPlanque},

        success: function (msg) {
            location.reload();

            $("#cadreReponse").html(msg)
        }
    })
})




// -------------------------- CUSTOM SELECT ---------------------------



function closeAllSelect(elmnt) {
    /* A function that will close all select boxes in the document,
    except the current select box: */
    let x, y, i, xl, yl, arrNo = [];
    x = document.getElementsByClassName("select-items");
    y = document.getElementsByClassName("select-selected");
    xl = x.length;
    yl = y.length;
    for (i = 0; i < yl; i++) {
        if (elmnt === y[i]) {
            arrNo.push(i)
        } else {
            y[i].classList.remove("select-arrow-active");
        }
    }
    for (i = 0; i < xl; i++) {
        if (arrNo.indexOf(i)) {
            x[i].classList.add("select-hide");
        }
    }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);



// ROTATION DU CUBE DE LINDEX

const defaultPerspective = '-150px';$(document).mousemove(rotateCube);function rotateCube(e) {
    const mouseX = e.pageX/getWidth();
    const mouseY = e.pageY/getHeight();
    const xDeg = getAngle(mouseX);
    const yDeg = getAngle(mouseY);let newStyle = `translateZ(${defaultPerspective}) rotateY(${xDeg}deg) rotateX(${yDeg}deg)`
    $('.cube').css('transform', newStyle);
}// this function return the corresponding angle for an x value
function getAngle(x) {
    return 45 - 90 * x;
}// Helper function to get page Width
function getWidth() {
    return Math.max(
        document.body.scrollWidth,
        document.documentElement.scrollWidth,
        document.body.offsetWidth,
        document.documentElement.offsetWidth,
        document.documentElement.clientWidth
    )
}// Helper function to get page Width
function getHeight() {
    return Math.max(
        document.body.scrollHeight,
        document.documentElement.scrollHeight,
        document.body.offsetHeight,
        document.documentElement.offsetHeight,
        document.documentElement.clientHeight
    )
}

// FIN ROTATION DU CUBE DE LINDEX


$('#submit2').click(function(){

    let ident = $('#emailConnec').val()
    let pass = $('#passConnec').val()

    $.ajax({
        type: "POST",
        url: "connexion.php",
        data : {'ident' : ident , 'pass' :pass },
        datatype: 'text',

        success: function(msg){
            if( msg == 'true'){
                window.location.replace("agent.php");
            }else{
                alert('Identifant ou Mot de passe incorrect');
            }







        }
    })

})